---
marp: true
title: Deep Learning on G5K
# theme: gaia
paginate: true
---

# Deep Learning on G5K

![](affiche_1112211.jpg)

### Presented by the SISR - LORIA | SED - INRIA

---

# Intro : How to run AI model on grid5000 

*What you will learn :* 
- How to *run* an AI model on **jupyterhub** on **CPU** and **GPU**
- How to *run* an AI model on **passive mode** with **oar** and a popular AI python template


*What you will **not** learn :* 
- How to create an AI algorithm 
- How to solve your AI problem


---


# Plan 

- Notebook in cpu/gpu
- From notebook to passive mode with 
    - [**lightning-hydra**](https://github.com/ashleve/lightning-hydra-template)  template
    - [**singularity**](https://github.com/sylabs/singularity) container
    - **oar** batch scheduler
- Run **multiple** experiments in passive mode 
- Visualize results with [mlflow]( https://mlflow.org/#features ) 
- Bonus : launch parallel experiments with **GNU parallel**
---


# The cutest AI task selected for this tutorial

**The Mandrillus Face Database:**
- Research project in ecology & evolutionary biology to study a natural population of mandrills located in Gabon 
- labels : identity / sex / age ...  
![](tuto_patchwork.png)


---

**What can we do with these mandrill faces ?** 

- predict the age of these cute baby mandrills

![](tuto_task.drawio.png)

---

# Let's go tuto : start with **notebooks**

## Torch lightning


### *lightning* hooks

- Classic pytorch loop : 


![alt text](ligntning_0step.png) 

---

- lightning wrap : 

[https://github.com/Lightning-AI/pytorch-lightning](https://github.com/Lightning-AI/pytorch-lightning)


<!-- ![alt text](pl_quick_start_full_compressed.gif) -->


---
## Follow the wiki
**https://gitlab.inria.fr/tutos-technos/ia_sur_g5k**


![](wiki_access.png)

For this live session, you can directly jump to the wiki page: 
**010_start_serveur_jupyterhub**

---

## Notebook summary 





*Best practices for developping notebooks*
1. Develop and debug notebooks on a **CPU** node 
2. Run the working notebook experiment on a **GPU** node

---

## Notebook summary 
*Limits* : 
- Interactive sessions: 
    - Risk of disconnection
    - Loss of work in progress
    - Ressources are locked **even when the notebook is not running**

- Notebooks : 
    - 1 notebook = 1 run : not adapted for big set of experiments 
    - Difficult to manage for reproductible experiments
    
---

# From notebooks to passive jobs


### *hydra* configuration


![alt text](hydra_torch_light.png) 


---

### Compose configuration files with Hydra
Folder hierarchy
```sh
config/
    model/
        resnet.yaml
        vgg.yaml
    train.yaml
mandrill/ # Lightning lib
train.py # Training script
```
---
### Compose configuration files with Hydra
The content of `train.yaml` is:
```yaml
# @package _global_

defaults:
  - _self_ # Include the parameter declared after defaults
  - model: null # Set the model to use
```

If we load the content of the train.yaml file with hydra we obtain the following configuration:
```yaml
model: null
```

---

### Overloading model config

Let's make a model: `config/model/vgg.yaml`
```yaml
# This is used to dynamically instanciate the class at runtime
_target_: mandrill.models.VGGFace 
# The number of conv filters at the first layer
start_filters: 64
# The vector size of the last layer 
output_dim: 2622 
```

---

### Overloading model config
We can now overload the model parameter by simply using:
```bash
python train.py model=vgg
```

It will gives us the following configuration:
```yaml
model:
    _target_: mandrill.models.VGGFace
    start_filters: 64
    output_dim: 2622
```
This is composition !
**How can we use it with a training script ?**

---

### Integrating Hydra+Lightning

```python
@hydra.main(
    version_base="1.3",
    config_path="config", # Folder where the config files are located
    config_name="train.yaml" # Name of the default config file
    )
def main(cfg: DictConfig):
    """Main entry point for training.

    :param cfg: DictConfig configuration composed by Hydra.
    """
    ...
    # Call the train method using the composed config file
    train(cfg)
    ...
```

---

### Intergrating Hydra+Lightning
```python
@task_wrapper
def train(cfg: DictConfig):
    # Instantiate the datamodule
    datamodule: LightningDataModule = hydra.utils.instantiate(cfg.datamodule)

    # Instantiate the model
    model: LightningModule = hydra.utils.instantiate(cfg.model)

    # Instantiate the trainer
    trainer: Trainer = hydra.utils.instantiate(cfg.trainer)

    # Train
    trainer.fit(model=model, datamodule=datamodule, ckpt_path=cfg.get("ckpt_path"))
```
Everything is instantiated based on the configuration files.
In the tutorial, we can start a training with:
```bash
python train.py experiment=mandrill_base
```

---

### Run jobs in passive mode with *singularity* and *oar*

-  **oar** is a ressource reservation system on g5k (https://www.grid5000.fr/w/Getting_Started)

- **Singularity** singularity is a containerization tool (similar to docker).

---

### Run jobs in passive mode with *singularity* and *oar*
`oar/reserve_node.sh`
```bash
...
oarsub \
-t inner=$INNER_ID\ # The id of the container job for this tutorial
-q production\ # The queue where the ressource is located
-l host=1/gpu=$gpu,walltime=$walltime\ # Specify the number of gpu and the max execution time
"${SCRIPT_DIR}/start_singularity.sh $@" # Command to run when we acquire the ressources
```

---

### Run jobs in passive mode with *singularity* and *oar*
`oar/start_singularity.sh`
```bash
...
module load singularity # Don't forget to load singularity module
singularity exec \
    --nv\ # Use gpu within container
    --bind $SOURCE_DIR:/ia_g5k\ # Bind the source dir in the container
    --bind $LOG_DIR:/ia_g5k/logs\ # Bind the log dir in the container
    --bind $DATA_DIR:/ia_g5k/data\ # Bind the data dir in the container
    $SINGULARITY_IMG_PATH bash $SCRIPT_DIR/train.sh $@ # Start the training script
```

`train.sh`
```bash
...
HYDRA_FULL_ERROR=1 python train.py $@
```

---

### Run jobs in passive mode with *singularity* and *oar*
- Next steps: **https://gitlab.inria.fr/tutos-technos/ia_sur_g5k/-/wikis/home**
    - Follow **2_passive_mode** and **3_visualization** on the wiki
    - Configure the parameters
    - Run an experiment with *oar* et *singularity* on a GPU
---

## Visualization with **mlflow**

We can visualize your results with **mlflow** !

The **node_server** where mlflow is running is : **grvingt-3**

`[http://localhost:8080/](http://localhost:8080/)`

Other logging tools exist such as **tensorboard** which can log locally. Some tools require you to log on the cloud such as Weights & Biases, Neptune... Be carefull where you log your work !

---

### Passive mode summary

- Passive mode:
    - Consumme ressources only during execution
    - Configuration files are saved for each run (reproducibility) 
    - Decoupling functionnalities and configurations
    - Can easily run jobs in parallel or explore hyperparameters

---

# Hyperparameter finetuning with Hydra

---

## Fine tune hyperparameters with Hydra
- Define an hyperparameter space
    - Learning rate: [0.001, 0.000001]
    - VGG start filters: {2, 4, 8, 16}
- You can also use any other parameter...
---

## Fine tune hyperparameters with Hydra

You can perform a grid search (run all possible combinaison)

```bash
python train.py -m experiment=mandrill_base model.lr=0.0001,0.001,0.01 model.backbone.start_filters=2,4,6
```

Which is equilavent to run
```bash
python train.py -m experiment=mandrill_base model.lr=0.0001 model.backbone.start_filters=2
python train.py -m experiment=mandrill_base model.lr=0.0001 model.backbone.start_filters=4
python train.py -m experiment=mandrill_base model.lr=0.0001 model.backbone.start_filters=6
python train.py -m experiment=mandrill_base model.lr=0.001 model.backbone.start_filters=2
...
```

---

## Fine tune hyperparameters with Hydra
This is done using a configuration file in `config/hparams_search/mandrill.yaml`

```yaml
optimized_metric: "val" # The metric to optimize
hydra:
    ...
    # define hyperparameter search space
    params:
      model.lr: interval(0.000001, 0.001)
      model.backbone.start_filters: choice(2, 4, 8, 16)
```

Then we run the search with:
```bash
python train.py -m experiment=mandrill_base hparams_search=mandrill
```

**All of this is done sequentially within the same process.**

---

## Fine tune hyperparameters with Hydra

If you have multiple experiments you can make multiple reservation for it
```bash
./oar/reserve_node.sh experiment=mandrill_base model.lr=0.0001 model.backbone.start_filters=2
./oar/reserve_node.sh experiment=mandrill_base model.lr=0.0001 model.backbone.start_filters=4
...
```

**Try to start a multirun either with one method or the other !**

Of course this can be scripted ! However in order to not overload OAR with a 1000 reservation, you can limit the ressources used with GNU parallel

---


## GNU parallel

GNU parallel allows you dispatch N jobs on a fixed amount of ressources.
Initial conditions:
- You have 9 jobs (3x3 test parameters)
- You only have 2 gpus

---

## GNU parallel
GNU parallel will create a queue and dispatch each job on a ressource when it is available.

You can find more information here : https://www.grid5000.fr/w/GNU_Parallel

![alt text](image.png)

---

## GNU parallel

- Record gpu ressources to a file name gpu-executors
`oarprint gpu -P gpudevice,cpuset,host -C+ -F "1/OAR_USER_GPUDEVICE=% OAR_USER_CPUSET=% oarsh %" | tee gpu-executors`

- Install env_parallel and follow instructions
`echo ". /usr/bin/env_parallel.bash" >> ~/.bashrc`

- Source to updated .bashrc
`source $HOME/.bashrc`

- Setup current env to record (all existing env var will be ignored). You can edit the ignored variable here: ~/.parallel/ignored_vars.
`env_parallel --record-env`

- Setup env variables that will be used by parallel
`source $HOME/ia_sur_g5k/oar/.env`

---

## GNU parallel

- Call the work using the gpu executors defined previously
```bash
env_parallel --env _\ # Load the env variables
    --slf gpu-executors\ # Specify the ressources used
    $HOME/ia_sur_g5k/oar/start_singularity.sh experiment=mandrill_base \
        trainer.max_epochs={1} learning_rate={2} ::: 10 20 ::: 0.000001 0.0001 0.001
```
You can play with the script `oar/reserve_gnu_parallel.sh` (parameters are hardcoded)

---

# Thank You !

---

### Connect to grid5000 

```bash
ssh <user>@access.grid5000.fr
<user>@access-north:~$ssh nancy
<user>@fnancy:~$
```
Or update config file `~/.ssh/config` & `ssh <user>@nancy.g5k`, see: 
 https://www.grid5000.fr/w/Tutorial_or_Teaching_Labs_HowTo 

And **tuto-techno** on the wiki : https://gitlab.inria.fr/tutos-technos/ia_sur_g5k/-/wikis/home 

---

### Run jobs in passive mode with *singularity* and *oar*
![](passive_pipeline.drawio.png)


---

## Tunnel SSH pour la visu **MlFlow**

```bash
oarsub -I -q production -l walltime=00:10:00
source .venvs/mandrill/bin/activate
cd  /srv/storage/tutotechno@storage4.nancy.grid5000.fr/log/mlflow
mlflow ui -p 8080 -h 0.0.0.0
```

Sur le client:
```bash
ssh -L 8080:<NODE>.nancy.grid5000.fr:8080 nancy.g5k
```
ou 

```bash
ssh -L 8080:<node_server>.nancy.grid5000.fr:8080 <login_g5k>@nancy -J <login_g5k>@access.grid5000.fr
```


- `<NODE> `dans cet exemple doit etre le noeud utilisé pour héberger le logiciel de visu)

- `<login_g5k> `: le login_g5k 

