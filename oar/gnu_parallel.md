# Usage example of gnu parallel on g5k

- Change directory to the correct folder

  `cd $HOME/ia_sur_g5k`

- Record gpu ressources to a file name gpu-executors

  `oarprint gpu -P gpudevice,cpuset,host -C+ -F "1/OAR_USER_GPUDEVICE=% OAR_USER_CPUSET=% oarsh %" | tee gpu-executors`

- Setup the env variables using env_parallel

  - Install env_parallel and follow instructions

    `echo ". /usr/bin/env_parallel.bash" >> ~/.bashrc`

  - Source to updated .bashrc

    `source $HOME/.bashrc`

  - Setup current env to record (all existing env var will be ignored). You can edit the ignored variable here: `~/.parallel/ignored_vars`.

    `env_parallel --record-env`

  - Setup env variables that will be used by parallel

    `source $HOME/ia_sur_g5k/oar/.env`

- Call the work using the gpu executors defined previously

  `env_parallel --env _ --slf gpu-executors $HOME/ia_sur_g5k/oar/start_singularity.sh experiment=mandrill_base trainer.max_epochs={1} learning_rate={2} ::: 10 20 ::: 0.000001 0.0001 0.001`
