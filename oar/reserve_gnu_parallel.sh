#!/bin/bash

set -x

walltime=00:10:00
gpu=4
host=2

source .env

oarsub -t inner=$INNER_ID -q production -l gpu=$gpu,walltime=$walltime "./run_gnu_parallel.sh"
