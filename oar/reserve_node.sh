#!/bin/bash

set -x

walltime=00:10:00
gpu=1

source .env

oarsub -t inner=$INNER_ID -q production -l host=1/gpu=$gpu,walltime=$walltime "${SCRIPT_DIR}/start_singularity.sh $@"
