#/bin/bash

set -x
source .env

module load singularity
singularity exec --nv\
    --bind $SOURCE_DIR:/ia_g5k\
    --bind $LOG_DIR:/ia_g5k/logs\
    --bind $DATA_DIR:/ia_g5k/data\
    $SINGULARITY_IMG_PATH bash $SCRIPT_DIR/train.sh $@
