#!/bin/bash

set -x

cd $HOME/ia_sur_g5k
oarprint gpu -P gpudevice,cpuset,host -C+ -F "1/OAR_USER_GPUDEVICE=% OAR_USER_CPUSET=% oarsh %" | tee gpu-executors
echo ". /usr/bin/env_parallel.bash" >> ~/.bashrc
source $HOME/.bashrc
env_parallel --record-env
source $HOME/ia_sur_g5k/oar/.env
env_parallel --env _ --slf gpu-executors $HOME/ia_sur_g5k/oar/start_singularity.sh experiment=mandrill_base trainer.max_epochs={1} learning_rate={2} ::: 10 20 ::: 0.000001 0.0001 0.001
