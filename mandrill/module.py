import numpy as np
import torch
import torch.nn.functional as F
import torchvision
from lightning import LightningModule
from mandrill.models import SequentialModel


class MandrillModule(LightningModule):
    # -------------------------------------------------------------------------
    # Init
    # -------------------------------------------------------------------------
    #
    def __init__(
        self,
        optimizer_class=None,
        lr=1e-4,
        batch_size=64,
        backbone=None,
        head=None,
        train_criterion=None,
        val_criterion=None,
        **kwargs,
    ):
        super().__init__()

        print("\n---- Mandrill module initialization --------------------------------------------")

        # ---- Hyperparameters
        #
        # Enable Lightning to store all the provided arguments under the self.hparams attribute.
        # These hyperparameters will also be stored within the model checkpoint.
        #
        self.save_hyperparameters(
            ignore=["optimizer_class", "backbone", "head", "train_criterion", "val_criterion"]
        )

        print("Hyperarameters are :")
        for name, value in self.hparams.items():
            print(f"{name:24s} : {value}")

        self.optimizer_class = optimizer_class
        self.backbone = backbone
        self.head = head
        self.head.set_input_dim(self.backbone.output_dim)
        self.train_criterion = train_criterion
        self.val_criterion = val_criterion

        self.model = SequentialModel(backbone=self.backbone, head=self.head)

    def forward(self, x):
        return self.model(x)

    def training_step(self, batch, batch_idx):
        x, y = batch
        outputs = self.forward(x)
        loss = self.train_criterion(outputs, y)
        self.log("loss", loss, prog_bar=True)
        return loss

    def validation_step(self, batch, batch_idx):
        x, y = batch
        outputs = self.forward(x)
        loss = self.val_criterion(outputs, y) * 365  # validation loss in days
        self.log("val", loss, prog_bar=True)
        return loss

    def configure_optimizers(self):
        lr = self.hparams.lr
        opt = self.optimizer_class(self.model.parameters(), lr=lr)
        return [opt], []

    def on_train_epoch_end(self):
        # Display some results maybe
        pass
