from mandrill.utils.instantiators import instantiate_callbacks, instantiate_loggers
from mandrill.utils.logging_utils import log_hyperparameters
from mandrill.utils.pylogger import RankedLogger
from mandrill.utils.rich_utils import enforce_tags, print_config_tree
from mandrill.utils.utils import extras, get_metric_value, task_wrapper

from torch.utils.data import DataLoader, random_split
import os
import torch
import numpy as np


def split_dataset(dataset, train_ratio, batch_size, num_workers=0):
    # Split the dataset into training and validation subsets
    train_size = int(train_ratio * len(dataset))
    val_size = len(dataset) - train_size
    train_dataset, val_dataset = random_split(dataset, [train_size, val_size])

    train_loader = DataLoader(
        train_dataset, batch_size=batch_size, shuffle=True, num_workers=num_workers
    )
    val_loader = DataLoader(
        val_dataset, batch_size=batch_size, shuffle=False, num_workers=num_workers
    )
    return train_loader, val_loader, train_dataset, val_dataset


def save(model, prefix, exp_name):
    torch.save(model.state_dict(), f"models/{prefix}_{exp_name}.h5")


def load(model, prefix, exp_name):
    path = f"models/{prefix}_{exp_name}.h5"
    if os.path.exists(path):
        model.load_state_dict(torch.load(path))
    else:
        print(f"Could not find path at : {path}")
    return model
