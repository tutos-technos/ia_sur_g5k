import warnings
from importlib.util import find_spec
from typing import Any, Callable, Dict, Optional, Tuple

import numpy as np
from matplotlib import pyplot as plt
from omegaconf import DictConfig

from mandrill.utils import pylogger, rich_utils

log = pylogger.RankedLogger(__name__, rank_zero_only=True)


def extras(cfg: DictConfig) -> None:
    """Applies optional utilities before the task is started.

    Utilities:
        - Ignoring python warnings
        - Setting tags from command line
        - Rich config printing

    :param cfg: A DictConfig object containing the config tree.
    """
    # disable python warnings
    if cfg.get("ignore_warnings"):
        log.info("Disabling python warnings! <cfg.extras.ignore_warnings=True>")
        warnings.filterwarnings("ignore")

    # prompt user to input tags from command line if none are provided in the config
    if cfg.get("enforce_tags"):
        log.info("Enforcing tags! <cfg.extras.enforce_tags=True>")
        rich_utils.enforce_tags(cfg, save_to_file=True)

    # pretty print config tree using Rich library
    if cfg.get("print_config"):
        log.info("Printing config tree with Rich! <cfg.extras.print_config=True>")
        rich_utils.print_config_tree(cfg, resolve=True, save_to_file=True)


def task_wrapper(task_func: Callable) -> Callable:
    """Optional decorator that controls the failure behavior when executing the task function.

    This wrapper can be used to:
        - make sure loggers are closed even if the task function raises an exception (prevents multirun failure)
        - save the exception to a `.log` file
        - mark the run as failed with a dedicated file in the `logs/` folder (so we can find and rerun it later)
        - etc. (adjust depending on your needs)

    Example:
    ```
    @utils.task_wrapper
    def train(cfg: DictConfig) -> Tuple[Dict[str, Any], Dict[str, Any]]:
        ...
        return metric_dict, object_dict
    ```

    :param task_func: The task function to be wrapped.

    :return: The wrapped task function.
    """

    def wrap(cfg: DictConfig) -> Tuple[Dict[str, Any], Dict[str, Any]]:
        # execute the task
        try:
            metric_dict, object_dict = task_func(cfg=cfg)

        # things to do if exception occurs
        except Exception as ex:
            # save exception to `.log` file
            log.exception("")

            # some hyperparameter combinations might be invalid or cause out-of-memory errors
            # so when using hparam search plugins like Optuna, you might want to disable
            # raising the below exception to avoid multirun failure
            raise ex

        # things to always do after either success or exception
        finally:
            # display output dir path in terminal
            log.info(f"Output dir: {cfg.paths.output_dir}")

            # always close wandb run (even if exception occurs so multirun won't fail)
            if find_spec("wandb"):  # check if wandb is installed
                import wandb

                if wandb.run:
                    log.info("Closing wandb!")
                    wandb.finish()

        return metric_dict, object_dict

    return wrap


def get_metric_value(metric_dict: Dict[str, Any], metric_name: Optional[str]) -> Optional[float]:
    """Safely retrieves value of the metric logged in LightningModule.

    :param metric_dict: A dict containing metric values.
    :param metric_name: If provided, the name of the metric to retrieve.
    :return: If a metric name was provided, the value of the metric.
    """
    if not metric_name:
        log.info("Metric name is None! Skipping metric value retrieval...")
        return None

    if metric_name not in metric_dict:
        raise Exception(
            f"Metric value not found! <metric_name={metric_name}>\n"
            "Make sure metric name logged in LightningModule is correct!\n"
            "Make sure `optimized_metric` name in `hparams_search` config is correct!"
        )

    print(metric_dict)
    print(metric_name)
    print(metric_dict[metric_name])
    metric_value = metric_dict[metric_name].item()
    log.info(f"Retrieved metric value! <{metric_name}={metric_value}>")

    return metric_value


# ==================================================================
#                                          _
#                                         | |
#              ___  ___ _ __ __ ___      _| | ___ _ __
#             / __|/ __| '__/ _` \ \ /\ / / |/ _ \ '__|
#             \__ \ (__| | | (_| |\ V  V /| |  __/ |
#             |___/\___|_|  \__,_| \_/\_/ |_|\___|_|
#
#                                                     fidle.Scrawler
# ==================================================================
# A simple module to host usefull functions for Fidle practical work
# Jean-Luc Parouty CNRS/MIAI/SIMaP 2022
# Contributed by Achille Mbogol Touye MIAI/SIMAP 2023 (PyTorch support)


import math
import sys

import matplotlib
import matplotlib.pyplot as plt
# from lightning.pytorch.callbacks.progress.base import ProgressBarBase
from tqdm import tqdm


# -------------------------------------------------------------
# Plot images
# -------------------------------------------------------------
#
def images(
    x,
    y=None,
    indices="all",
    columns=12,
    x_size=1,
    y_size=1,
    colorbar=False,
    y_pred=None,
    cm="binary",
    norm=None,
    y_padding=0.35,
    spines_alpha=1,
    fontsize=20,
    interpolation="lanczos",
    save_as="auto",
    show=False,
):
    """
    Show some images in a grid, with legends
    args:
        x             : images - Shapes must be (-1,lx,ly) (-1,lx,ly,1) or (-1,lx,ly,3),(-1,1,lx,ly) or (-1,3,lx,ly)
        y             : real classes or labels or None (None)
        indices       : indices of images to show or 'all' for all ('all')
        columns       : number of columns (12)
        x_size,y_size : figure size (1), (1)
        colorbar      : show colorbar (False)
        y_pred        : predicted classes (None)
        cm            : Matplotlib color map (binary)
        norm          : Matplotlib imshow normalization (None)
        y_padding     : Padding / rows (0.35)
        spines_alpha  : Spines alpha (1.)
        font_size     : Font size in px (20)
        save_as       : Filename to use if save figs is enable ('auto')
    returns:
        nothing
    """
    if indices == "all":
        indices = range(len(x))
    if norm and len(norm) == 2:
        norm = matplotlib.colors.Normalize(vmin=norm[0], vmax=norm[1])
    draw_labels = y is not None
    draw_pred = y_pred is not None
    # Torch Tensor ?
    if y.__class__.__name__ == "Tensor":
        y = y.numpy()
    if y_pred.__class__.__name__ == "Tensor":
        y_pred = y_pred.detach().numpy()

    rows = math.ceil(len(indices) / columns)
    fig = plt.figure(figsize=(columns * x_size, rows * (y_size + y_padding)))
    n = 1
    for i in indices:
        axs = fig.add_subplot(rows, columns, n)
        n += 1
        # ---- Shape is (lx,ly)
        if len(x[i].shape) == 2:
            xx = x[i]
        # ---- Shape is (lx,ly,c) or (c,lx,ly)
        if len(x[i].shape) == 3:
            if x[i].__class__.__name__ == "Tensor":
                (c, lx, ly) = x[i].shape
                if c == 1:
                    xx = x[i].permute(1, 2, 0).numpy().reshape(lx, ly)
                else:
                    xx = x[i].permute(1, 2, 0).numpy()  # ---> (lx,ly,n)
            else:
                (lx, ly, c) = x[i].shape
                if c == 1:
                    xx = x[i].reshape(lx, ly)
                else:
                    xx = x[i]

        img = axs.imshow(xx, cmap=cm, norm=norm, interpolation=interpolation)
        axs.spines["right"].set_visible(True)
        axs.spines["left"].set_visible(True)
        axs.spines["top"].set_visible(True)
        axs.spines["bottom"].set_visible(True)
        axs.spines["right"].set_alpha(spines_alpha)
        axs.spines["left"].set_alpha(spines_alpha)
        axs.spines["top"].set_alpha(spines_alpha)
        axs.spines["bottom"].set_alpha(spines_alpha)
        axs.set_yticks([])
        axs.set_xticks([])
        if draw_labels and not draw_pred:
            axs.set_xlabel(y[i], fontsize=fontsize)
        if draw_labels and draw_pred:
            if y[i] != y_pred[i]:
                axs.set_xlabel(f"{y_pred[i]} ({y[i]})", fontsize=fontsize)
                axs.xaxis.label.set_color("red")
            else:
                axs.set_xlabel(y[i], fontsize=fontsize)
        if colorbar:
            fig.colorbar(img, orientation="vertical", shrink=0.65)
    plt.savefig(f"{save_as}.png")
    if show:
        plt.show()

def display_validation(model, dataloader, n_display=10):
    # Perform inference on validation images
    n_images = 0
    for i, (images, targets) in enumerate(dataloader):
        # Forward pass
        outputs = model(images)
    
        for j in range(outputs.shape[0]):
            # Convert the outputs to numpy arrays
            predicted_values = outputs[j].squeeze().detach().cpu().numpy() * 365
            actual_values = targets[j].squeeze().cpu().numpy() * 365
        
            if n_images >= n_display:
                continue
    
            n_images += 1
    
            # Display the results
            print("Predicted Values:", predicted_values)
            print("Actual Values:", actual_values)
            print("Prediction Error: ", predicted_values - actual_values)
            print()  # Add an empty line for separation
            
            # Visualize the images and predictions
            print(images[j].shape)
            im = images[j].squeeze().cpu().permute(1, 2, 0).numpy()
            im = (im * 255).astype(np.uint8)
            b,g,r = im[:,:,0],im[:,:,1],im[:,:,2]
            im = np.stack([r,g,b], axis=-1)
            plt.imshow(im)
            plt.title(f"Predicted: {predicted_values}, Actual: {actual_values}")
            plt.show()