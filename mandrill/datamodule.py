from lightning import LightningDataModule
from mandrill.dataset import read_dataset, MandrillImageDataset
from mandrill.utils import split_dataset


class MandrillDataModule(LightningDataModule):
    def __init__(
        self,
        images_dir,
        csv_path,
        max_days,
        in_mem=True,
        batch_size=64,
        num_workers=4,
        filter_dob_error=False,
        filter_certainty=False,
        train_ratio=0.8,
    ):
        super().__init__()

        print("\n---- MandrillDataModule initialization ----------------------------")

        self.images_dir = images_dir
        self.csv_path = csv_path
        self.max_days = max_days
        self.in_mem = in_mem
        self.filter_dob_error = filter_dob_error
        self.filter_certainty = filter_certainty
        self.batch_size = batch_size
        self.num_workers = num_workers
        self.train_ratio = train_ratio

        self.dims = (28, 28, 1)
        self.num_classes = 10

    def prepare_data(self):
        # Read and filter the data
        data = read_dataset(
            self.csv_path,
            filter_dob_error=self.filter_dob_error,
            filter_certainty=self.filter_certainty,
            max_age=self.max_days,
        )
        # Create a dataset
        dataset = MandrillImageDataset(
            root_dir=self.images_dir, dataframe=data, in_mem=self.in_mem, max_days=self.max_days
        )

        train_loader, val_loader, train_dataset, val_dataset = split_dataset(
            dataset, self.train_ratio, self.batch_size
        )

        self.train_loader = train_loader
        self.train_dataset = train_dataset
        self.val_loader = val_loader
        self.val_dataset = val_dataset

    def setup(self, stage=None):
        pass

    def train_dataloader(self):
        return self.train_loader

    def val_dataloader(self):
        return self.val_loader
