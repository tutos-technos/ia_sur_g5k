import logging
from collections import OrderedDict

import os
import timm

import torch
import torch.nn.functional as F
import torch.nn as nn
import torchfile

log = logging.getLogger(__name__)


class HuggingFaceModel(nn.Module):
    def __init__(self, name):
        super(HuggingFaceModel, self).__init__()
        self.name = name
        self.backbone = timm.create_model(
            name,
            pretrained=False,
            num_classes=0,
        )
        self.output_dim = self.backbone.num_features

    def load_weights(self, path):
        if os.path.exists(path):
            try:
                import safetensors

                log.info("Loading pretrained weights...")
                safetensors.torch.load_file(path, device="cuda")
            except ImportError:
                log.info("Failed to load safetensors")


class SequentialModel(nn.Module):
    def __init__(self, backbone, head):
        super(SequentialModel, self).__init__()
        self.backbone = backbone
        self.head = head

    def forward(self, x):
        x = self.backbone(x)
        x = self.head(x)
        return x


def build_layers(n, method, prev_features, current_features, down=True):
    layers = OrderedDict()
    for i in range(n):
        layers[str(i)] = method(prev_features, current_features)
        prev_features = current_features
        if down:
            current_features = current_features // 2
        else:
            current_features = current_features * 2
    return layers, prev_features


class RegressionHead(nn.Module):
    def __init__(
        self,
        input_dim=1,
        output_dim=1,
        lin_start=2048,
        n_lin=6,
        sigmoid=False,
        dropout=0.0,
        use_norm=False,
    ):
        super(RegressionHead, self).__init__()

        self.init = False
        self.lin_start = lin_start
        self.n_lin = n_lin
        self.sigmoid = sigmoid
        self.dropout = dropout
        self.use_norm = use_norm
        self.output_dim = output_dim
        self.input_dim = input_dim

    def set_input_dim(self, input_dim):
        previous_feature_size = input_dim
        current_feature_size = self.lin_start
        last_feature_size = input_dim
        self.blocks = None
        if self.n_lin > 0:
            lin_layers, last_feature_size = build_layers(
                self.n_lin, self.block, previous_feature_size, current_feature_size
            )
            self.blocks = nn.Sequential(lin_layers)
        self.input_dim = input_dim
        self.last_feature_size = last_feature_size
        self.linear = nn.Linear(self.last_feature_size, self.output_dim, bias=True)

        if self.sigmoid:
            self.activation = nn.Sigmoid()
        else:
            self.activation = None
        self.zero_tensor = torch.tensor(0.0)

        self.norm = nn.LayerNorm(last_feature_size) if self.use_norm else nn.Identity()
        self.dropout = nn.Dropout(p=self.dropout)

        self.init = True

    def update_input_dim(self, output_dim):
        self.output_dim = output_dim

        print(self.linear)

    def block(self, in_features, out_features):
        lin = nn.Linear(in_features, out_features)
        gelu = nn.GELU()
        return nn.Sequential(lin, gelu)

    def _init_weights(self, m):
        if isinstance(m, nn.Linear):
            nn.init.trunc_normal_(m.weight, std=0.02)
            if isinstance(m, nn.Linear) and m.bias is not None:
                nn.init.constant_(m.bias, 0)

    def forward(self, x):
        if not self.init:
            raise ValueError("Regression head must be initialised with set_input_dim() function")

        if self.blocks:
            x = self.blocks(x)
        x = self.norm(x)
        x = self.dropout(x)
        x = self.linear(x)

        if self.output_dim == 1:
            x = torch.reshape(x, (x.shape[0],))

        if self.activation:
            x = self.activation(x)

        return x


class VGGFace(nn.Module):
    def __init__(self, start_filters=64, output_dim=2622):
        super().__init__()

        self.start_filters = start_filters
        self.block_size = [2, 2, 3, 3, 3]
        self.conv_1_1 = nn.Conv2d(3, start_filters, 3, stride=1, padding=1)
        self.conv_1_2 = nn.Conv2d(start_filters, start_filters, 3, stride=1, padding=1)
        self.conv_2_1 = nn.Conv2d(start_filters, start_filters * 2, 3, stride=1, padding=1)
        self.conv_2_2 = nn.Conv2d(start_filters * 2, start_filters * 2, 3, stride=1, padding=1)
        self.conv_3_1 = nn.Conv2d(start_filters * 2, start_filters * 4, 3, stride=1, padding=1)
        self.conv_3_2 = nn.Conv2d(start_filters * 4, start_filters * 4, 3, stride=1, padding=1)
        self.conv_3_3 = nn.Conv2d(start_filters * 4, start_filters * 4, 3, stride=1, padding=1)
        self.conv_4_1 = nn.Conv2d(start_filters * 4, start_filters * 8, 3, stride=1, padding=1)
        self.conv_4_2 = nn.Conv2d(start_filters * 8, start_filters * 8, 3, stride=1, padding=1)
        self.conv_4_3 = nn.Conv2d(start_filters * 8, start_filters * 8, 3, stride=1, padding=1)
        self.conv_5_1 = nn.Conv2d(start_filters * 8, start_filters * 8, 3, stride=1, padding=1)
        self.conv_5_2 = nn.Conv2d(start_filters * 8, start_filters * 8, 3, stride=1, padding=1)
        self.conv_5_3 = nn.Conv2d(start_filters * 8, start_filters * 8, 3, stride=1, padding=1)
        self.fc6 = nn.Linear(start_filters * 8 * 7 * 7, start_filters * 64)
        self.fc7 = nn.Linear(start_filters * 64, start_filters * 64)
        self.fc8 = nn.Linear(start_filters * 64, output_dim)
        self.create_bn()

        self.last_output_dim = output_dim
        self.output_dim = start_filters * 8 * 7 * 7

    def load_weights(self, path="data/pretrained/VGG_FACE.t7"):
        """Function to load luatorch pretrained

        from: https://www.robots.ox.ac.uk/~vgg/software/vgg_face/

        Args:
            path: path for the luatorch pretrained
        """
        assert (
            self.start_filters == 64 and self.last_output_dim == 2622
        ), "You must use the correct model size to load the pretrained weight."
        model = torchfile.load(path)
        counter = 1
        block = 1
        for i, layer in enumerate(model.modules):
            if layer.weight is not None:
                if block <= 5:
                    self_layer = getattr(self, "conv_%d_%d" % (block, counter))
                    counter += 1
                    if counter > self.block_size[block - 1]:
                        counter = 1
                        block += 1
                    self_layer.weight.data[...] = torch.tensor(layer.weight).view_as(
                        self_layer.weight
                    )[...]
                    self_layer.bias.data[...] = torch.tensor(layer.bias).view_as(self_layer.bias)[
                        ...
                    ]
                else:
                    self_layer = getattr(self, "fc%d" % (block))
                    block += 1
                    self_layer.weight.data[...] = torch.tensor(layer.weight).view_as(
                        self_layer.weight
                    )[...]
                    self_layer.bias.data[...] = torch.tensor(layer.bias).view_as(self_layer.bias)[
                        ...
                    ]
        print(f"Loaded model from pretrained model at path : {path}")

    def create_bn(self):
        self.bn_1_1 = nn.BatchNorm2d(self.start_filters)
        self.bn_1_2 = nn.BatchNorm2d(self.start_filters)
        self.bn_2_1 = nn.BatchNorm2d(self.start_filters * 2)
        self.bn_2_2 = nn.BatchNorm2d(self.start_filters * 2)
        self.bn_3_1 = nn.BatchNorm2d(self.start_filters * 4)
        self.bn_3_2 = nn.BatchNorm2d(self.start_filters * 4)
        self.bn_3_3 = nn.BatchNorm2d(self.start_filters * 4)
        self.bn_4_1 = nn.BatchNorm2d(self.start_filters * 8)
        self.bn_4_2 = nn.BatchNorm2d(self.start_filters * 8)
        self.bn_4_3 = nn.BatchNorm2d(self.start_filters * 8)
        self.bn_5_1 = nn.BatchNorm2d(self.start_filters * 8)
        self.bn_5_2 = nn.BatchNorm2d(self.start_filters * 8)
        self.bn_5_3 = nn.BatchNorm2d(self.start_filters * 8)
        self.bn_6 = nn.BatchNorm1d(self.start_filters * 64)
        self.bn_7 = nn.BatchNorm1d(self.start_filters * 64)

    def features(self, x):
        x = self.bn_1_1(F.relu(self.conv_1_1(x)))
        x = self.bn_1_2(F.relu(self.conv_1_2(x)))
        x = F.max_pool2d(x, 2, 2)  # 224 -> 112
        x = self.bn_2_1(F.relu(self.conv_2_1(x)))
        x = self.bn_2_2(F.relu(self.conv_2_2(x)))
        x = F.max_pool2d(x, 2, 2)  # 112 -> 56
        x = self.bn_3_1(F.relu(self.conv_3_1(x)))
        x = self.bn_3_2(F.relu(self.conv_3_2(x)))
        x = self.bn_3_3(F.relu(self.conv_3_3(x)))
        x = F.max_pool2d(x, 2, 2)  # 56 -> 28
        x = self.bn_4_1(F.relu(self.conv_4_1(x)))
        x = self.bn_4_2(F.relu(self.conv_4_2(x)))
        x = self.bn_4_3(F.relu(self.conv_4_3(x)))
        x = F.max_pool2d(x, 2, 2)  # 28 -> 14
        x = self.bn_5_1(F.relu(self.conv_5_1(x)))
        x = self.bn_5_2(F.relu(self.conv_5_2(x)))
        x = self.bn_5_3(F.relu(self.conv_5_3(x)))
        x = F.max_pool2d(x, 2, 2)  # 14 -> 7
        x = x.view(
            x.size(0), -1
        )  # 7x7x512 => This part is used as features for age regression in deepface
        # x = self.bn_6(F.relu(self.fc6(x)))
        # x = F.dropout(x, 0.5, self.training)
        # x = self.bn_7(F.relu(self.fc7(x)))
        # x = F.dropout(x, 0.5, self.training)
        # x = self.fc8(x)

        # x = torch.flatten(x, start_dim=1)
        return x

    def forward(self, x):
        x = self.features(x)
        return x
