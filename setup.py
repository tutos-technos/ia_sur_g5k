#!/usr/bin/env python

from distutils.core import setup

setup(
    name="mandrill",
    version="1.0",
    description="Mandrill age regression example for the ia g5k tutorial",
    packages=["mandrill"],
)
